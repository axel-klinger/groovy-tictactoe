
class TicTacToe {

  static main(args) {

    def spielfeld = ['X', ' ', ' ', ' ', 'O', ' ', ' ', ' ', ' ']

    printSpielfeld(spielfeld)
  }

  static printSpielfeld(spielfeld) {
    println spielfeld[0] + " | " + spielfeld[1] + " | " + spielfeld[2]
    println "- + - + -"
    println spielfeld[3] + " | " + spielfeld[4] + " | " + spielfeld[5]
    println "- + - + -"
    println spielfeld[6] + " | " + spielfeld[7] + " | " + spielfeld[8]
  }
}
